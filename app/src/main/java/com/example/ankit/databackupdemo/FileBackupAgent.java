package com.example.ankit.databackupdemo;

import android.app.backup.BackupAgentHelper;
import android.app.backup.FileBackupHelper;

//TODO: writing and reading files are not thread safe so use synchronized blocks
public class FileBackupAgent extends BackupAgentHelper {
    // The name of the fileo
    static final String TOP_SCORES = "scores";
    static final String PLAYER_STATS = "stats";

    // A key to uniquely identify the set of backup data
    static final String FILES_BACKUP_KEY = "myfiles";

    // Allocate a helper and add it to the backup agent
    @Override
    public void onCreate() {
        FileBackupHelper helper = new FileBackupHelper(this, TOP_SCORES, PLAYER_STATS);
        addHelper(FILES_BACKUP_KEY, helper);
    }
}
