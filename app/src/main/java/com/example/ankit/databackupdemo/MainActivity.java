package com.example.ankit.databackupdemo;

import android.app.backup.BackupManager;
import android.app.backup.RestoreObserver;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText mETName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mETName = (EditText) findViewById(R.id.et_name);

        if (mETName != null) {
            mETName.setText(getSharedPreferences("PREF", MODE_PRIVATE).getString("NAME", ""));
        }
    }

    public void saveData(View view) {

        getSharedPreferences("PREF", MODE_PRIVATE)
                .edit()
                .putString("NAME", mETName.getText().toString())
                .commit();

        Toast.makeText(this, "Saved", Toast.LENGTH_SHORT).show();

        new BackupManager(this).dataChanged();
        /*new BackupManager(this).requestRestore(new RestoreObserver() {
            @Override
            public void restoreStarting(int numPackages) {
                super.restoreStarting(numPackages);
            }

            @Override
            public void onUpdate(int nowBeingRestored, String currentPackage) {
                super.onUpdate(nowBeingRestored, currentPackage);
            }

            @Override
            public void restoreFinished(int error) {
                super.restoreFinished(error);
            }
        });*/
    }
}
